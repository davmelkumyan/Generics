﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generic_8
{
    class Program
    {
        static void Main(string[] args)
        {
            Shape shape = new Circle();
            IContainer<Circle> container = new Container<Shape>(shape);
        }
    }

    class Shape { }
    class Circle : Shape { }

    interface IContainer<in T>
    {
        T Field { set; }
    }

    class Container<T> : IContainer<T>
    {
        public T field;

        public Container(T field)
        {
            this.field = field;
        }

        public T Field
        {
            set { field = value; }
        }
    }
}

    
