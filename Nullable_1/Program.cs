﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nullable_1
{
    class Program
    {
        static void Main(string[] args)
        {
            int? a = null; // Equal to -->    Nullable<int> a = null;
            int? b = -5;

            if(a > b)
            {
                Console.WriteLine("Всегда false");
            }

            b = null;
            if(a == b)
            {

            }

        }
    }
}
