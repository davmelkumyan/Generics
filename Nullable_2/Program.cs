﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nullable_2
{
    class Program
    {
        static void Main(string[] args)
        {
            int? a = null;
            int? b = 10;
            int? c = a ?? b; // Поглощение 

            c = (a != null) ? a : b; // Эквивалентен ??

            a = 3;
            c = (a != null) ? a : b; // c = 3
            c = a ?? b; // c = 3;
            
        }
    }
}
