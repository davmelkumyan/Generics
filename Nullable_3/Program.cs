﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nullable_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Nullable<int> a = null;
            Nullable<int> b = 10;

            if (!a.HasValue)
            {
                Console.WriteLine("Is a null");
            }
            if (b.HasValue)
            {
                Console.WriteLine("Is not null");
            }
        }
    }
}
