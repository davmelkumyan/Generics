﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generics
{
    class Program
    {
        static void Main(string[] args)
        {
            MyClass<int> instance = new MyClass<int>();
        }
    }

    class MyClass<T>
    {
        public T field;

        public void GetField()
        {
            Console.WriteLine(field.GetType());
        }
    }
}
