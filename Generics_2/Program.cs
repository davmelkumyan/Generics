﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generics_2
{
    class Program
    {
        static void Main(string[] args)
        {
            MyClass<int, int> instance = new MyClass<int, int>(10, 20);
            Console.WriteLine(instance.Variable1 + instance.Variable2);

            MyClass<string, string> instance2 = new MyClass<string, string>("Hello", "David");
            Console.WriteLine(instance2.Variable1 + instance2.Variable2);
        }
    }

    class MyClass<T1, T2>
    {
        private T1 variable1;
        private T2 variable2;

        public MyClass(T1 field1, T2 field2)
        {
            this.variable1 = field1;
            this.variable2 = field2;
        }

        public T1 Variable1
        {
            get => variable1;
            set => variable1 = value;
        }

        public T2 Variable2
        {
            get => variable2;
            set => variable2 = value;
        }
    }
}
