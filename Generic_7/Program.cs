﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generic_7
{
    class Program
    {
        static void Main(string[] args)
        {
            Cicle cicle = new Cicle();
            IContainer<Shape> container = new Container<Cicle>(cicle);
        }
    }

    class Shape { }
    class Cicle : Shape { }

    interface IContainer<out T>
    {
        T Field { get; }
    }

    class Container<T> : IContainer<T>
    {
        public T field;
        public Container(T field)
        {
            this.field = field;
        }
       public T Field { get => this.field; }
    }
}
