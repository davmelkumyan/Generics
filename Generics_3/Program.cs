﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generics_3
{
    class Program
    {
        static void Main(string[] args)
        {
            string name = "David";
            new MyClass().Method<string>(name);
            new MyClass().Method(name);
        }
    }

    class MyClass
    {
        public void Method<T>(T argument)
        {
            Console.WriteLine("Text + " + argument);
        }
    }
}
