﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generics_5
{
    class Program
    {
        static void Main(string[] args)
        {
            Circle circle = new Circle();
            IContainer<Circle> container = new Container<Circle>(circle);
            Console.WriteLine(container.Figure.ToString());
        }
    }

    abstract class Shape { }
    class Circle : Shape { }

    interface IContainer<T>
    {
        T Figure { get; set; }
    }

    class Container<T> : IContainer<T>
    {
        public T Figure { get; set; }

        public Container(T figure)
        {
            this.Figure = figure;
        }
    }
}
