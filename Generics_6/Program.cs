﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generics_6
{
    class Program
    {
        static void Main(string[] args)
        {
            Circle circle = new Circle();
            IContainer<Shape> container = new Container<Shape>(circle);
        }
    }

    abstract class Shape { }
    class Circle : Shape { }

    interface IContainer<T>
    {
        T Figure { get; set; }
    }

    class Container<T> : IContainer<T>
    {
        public T Figure { get; set; }

        public Container(T figure)
        {
            this.Figure = figure;
        }
    }

}
